import React from 'react';
import { Link } from 'react-router-dom';

import './App.css';

function App() {
  return (
    <div className="App">
      <h1>This is Home Page</h1>
      <div>
        <Link to="/signup">SignUp</Link> |  <Link to="/login">Login</Link>
      </div>
    </div>
  );
}

export default App;
