import React, { Component } from 'react';

class FormComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    handleChange = (e) => {
        this.setState({ 
            [e.target.id]: e.target.value 
        });
     }

    handleSubmit = (e) => {
        e.preventDefault();
        Object.keys(this.state).forEach(key => {
            console.log(`${key} : ${this.state[key]}`);
        });
    }

    componentDidMount = () => {
        Object.values(this.props).forEach(element => {
            element.forEach((e, index) => {
                
                if (e.type === 'email' || e.type === 'password' || e.type === 'checkbox')
                    {
                        this.setState({
                            [`${e.type}${index}`]: e.defaultValue ? e.defaultValue : ''
                        })
                    }
                if (e.type === 'number')
                    {
                        this.setState({
                            [`${e.type}${index}`]: e.defaultValue ? e.defaultValue : 0
                        })
                    }
            });
        });
    }

    shouldComponentUpdate = () => {
        return false;
    }

    render() {
        console.log("hi");
        const propsValue = this.props[Object.keys(this.props)[0]];

        return (
            <div>
                <form>
                    {
                        propsValue && propsValue.length > 0 ? propsValue.map((element, index) => {
                            
                            if (element.type !== 'checkbox')
                                {
                                    return (
                                        <div className="form-group" key={index}>
                                            <div>
                                                <label htmlFor={element.type + Number(index).toString()}>{element.labelName}</label>
                                                <input type={element.type} className="form-control" id={element.type + Number(index).toString()}
                                                        placeholder={element.defaultValue === undefined || element.defaultValue === '' ? 
                                                        `Enter ${element.type}`: null}
                                                        defaultValue={this.state[`${element.type}${index}`]}
                                                        ref={(input) => this[element.type + Number(index).toString()] = input}
                                                        onChange={this.handleChange}
                                                />
                                            </div>
                                        </div>
                                    )
                                }
                            else
                                {
                                    return (
                                        <div className="form-group form-check" key={index}>
                                            <div>
                                                <input type={element.type} className="form-check-input" id={element.type + Number(index).toString()} 
                                                        defaultValue={this.state[`${element.type}${index}`]} 
                                                        ref={(input) => this[element.type + Number(index).toString()] = input}
                                                />
                                                <label htmlFor={element.type + Number(index).toString()} className="form-check-label">
                                                        {element.labelName}
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }
                            }) : propsValue ? `${Object.keys(this.props)[0]} is empty. Check again` : "Props are undefined"
                    }
                </form>
                <button type="submit" className={propsValue && propsValue.length > 0 ? "btn btn-primary" : "d-none"} onClick={this.handleSubmit}>
                    Submit
                </button>
            </div>
        );
    }
}
 
export default FormComponent;