import React, { Component } from 'react';
import FormComponent from './FormComponent';

class Login extends Component {
    state = {
        formDetails: [
            {
                type: 'email',
                labelName: 'Email Address',
                defaultValue: ''
            },
            {
                type: 'password',
                labelName: 'Password',
                defaultValue: ''
            },
            {
                type: 'password',
                labelName: 'Confirm Password',
                defaultValue: ''
            }
        ]
    }
    render() { 
        return (
            <div>
                <h1>Login Page</h1>
                <FormComponent formDetails={this.state.formDetails} />
            </div>
        );
    }
}
 
export default Login;