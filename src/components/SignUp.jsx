import React, { Component } from 'react';
import FormComponent from './FormComponent';

class SignUp extends Component {
    state = {
        formDetails: [
            {
                type: 'email',
                labelName: 'Email Address',
                defaultValue: 'abc@abc.com'
            },
            {
                type: 'password',
                labelName: 'Password',
                defaultValue: ''
            },
            {
                type: 'number',
                labelName: 'Age',
                defaultValue: 0
            },
            {
                type: 'checkbox',
                labelName: 'I have read the terms and conditions',
                defaultValue: ''
            }
        ]
    }
    render() { 
        return (
            <div>
                <h1>SignUp Page</h1>
                <FormComponent formDetails={this.state.formDetails} />
            </div>
            
        );
    }
}
 
export default SignUp;